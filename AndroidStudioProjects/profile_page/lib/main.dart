import 'package:flutter/material.dart';

void main() {
  runApp(ContactProfilePage());
}

class ContactProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.white,
            leading: Icon(
              Icons.arrow_back,
              color: Colors.black,
            ),
            actions: <Widget>[
              IconButton(
                  onPressed: () {},
                  icon: Icon(Icons.star_border),
                  color: Colors.black),
            ]),
        body: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  height: 300,
                  color: Colors.amber[600],
                  child: Image.network(
                    "https://media.discordapp.net/attachments/678590561002717225/1047573726163652748/549_20220917050606-2.png?width=676&height=676",
                    fit: BoxFit.cover,
                  ),
                ),
                Container(
                    height: 80,
                    //color: Colors.amber[500],
                    child: Row(
                      children: [
                        Padding(
                          padding: EdgeInsets.all(20),
                          child: Text(
                            'Cigarrette',
                            style: TextStyle(fontSize: 30),
                          ),
                        ),
                      ],
                    )),
                Divider(
                  color: Colors.grey,
                ),
                Container(
                  margin: EdgeInsets.only(top: 8, bottom: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      buildCallButton(),
                    ],
                  ),
                ),
                Divider(
                  color: Colors.grey,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}